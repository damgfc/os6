#ifndef SHARED_H
#define SHARED_H

#include <semaphore.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_PROCESSES 18
#define NUM_PROCESSES 12
#define BUFFER 128
#define PAGESIZE 256

sem_t mutex; /* access to cs */
sem_t empty; /* num available buffers */
sem_t full; /* init to 0 */

typedef struct {
	int max;
	int vAddress[32];
	int write; // 1 if writable, else 0
} pageTable;

typedef struct {
	int pidArray[MAX_PROCESSES];
	int bitVector[MAX_PROCESSES]; // 0 if no process, 1 if process
	int processes; //Number of processes used
	unsigned int clockSeconds;  //in seconds
	unsigned int clockMilliseconds; //in milliseconds
	pageTable pages[MAX_PROCESSES];
	
} sharedStruct;


int detatchAndRemove(int shmid, void *shmaddr);
void signalHandling(int sigNum);
void incrementTime(unsigned int seconds, unsigned int milliseconds);
int createSharedMemory();

#endif
