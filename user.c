/*
$Author: o-moring $
$Log: user.c,v $
Revision 1.2  2015/04/30 01:09:18  o-moring
fixed issues with shm & added semaphores

Revision 1.1  2015/04/20 04:19:37  o-moring
Initial revision

$Date: 2015/04/30 01:09:18 $
*/

#include <stdio.h>
#include <unistd.h>
#include "shared.h"

sharedStruct *shmP;
sem_t *pSem;

int main(int argc, char **argv){
	int max, process, key, i, page, numRef;
	char procName[20];
	numRef = 0;
	if (argc != 2){
		perror("Please provide a parameter");
		exit(1);
	}
	
	process = atoi(argv[1]);

	/*** Create and Attach shared memory ***/
	key = ftok("./ftokFile", 42);
	if (key == -1){
		perror("ftok failed\n");
		return -1;
	}
	
	int shmid = shmget(key, sizeof(sharedStruct), 0777);
	
	if (shmid == -1){
		perror("Error in child shmget");
		exit(1);
	}
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));
	
	shmP->pidArray[process] = getpid();
	
	/*** Get the named semaphores ***/
	sprintf(procName, "damUser%d", process);
	if((pSem = sem_open(procName, 1)) == SEM_FAILED) {
		printf("Failed opening sem in process %d", process);
	}

	srand(time(NULL));
	/*** Get max memory ***/
	max = rand() % MAX_PROCESSES + 1;
	shmP->pages[process].max = max;
	
	for (i = 0; i < max; i++){
		shmP->pages[process].vAddress[i] = 0;
	}
	
	while (1){
		shmP->pages[process].write = rand() % 2;
		page = rand() % max + 1;
		numRef++;
		
		sem_wait(pSem);
		
		if (numRef >= 9000){
			//check to see if we should die? YES
			int die = rand() % 2;
			if (die == 1){
				exitUser(process);	
			}
		}
	}
}

int exitUser(int process){
	sem_close(pSem);
	shmP->pidArray[process] = 0;
	shmP->bitVector[process] = 0;
	shmP->processes--;
	exit(0);
}
