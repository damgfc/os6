/*
$Author: o-moring $
$Log: oss.c,v $
Revision 1.2  2015/04/30 01:09:18  o-moring
fixed issues with shm & added semaphores

Revision 1.1  2015/04/20 04:19:37  o-moring
Initial revision

$Date: 2015/04/30 01:09:18 $
*/
#include <stdio.h>
#include "shared.h"

sharedStruct *shmP;
int fifo[MAX_PROCESSES];
int memVector[MAX_PROCESSES];
int memAlloc[MAX_PROCESSES];
int referenceVector[MAX_PROCESSES];
int dirtyBitVector[MAX_PROCESSES];
sem_t *processSem[NUM_PROCESSES];

int main(int argc, char **argv){
	int key, i, ms, total;
	char userArg[3], semaphoreName[20];
	total = 0;
	
	/*** Signal Handling ***/
	signal(SIGINT, signalHandling);
	signal(SIGSEGV, signalHandling);
	
	/*** Create and Attach Memory ***/
	key = ftok("./ftokFile", 42);
	if (key == -1){
		perror("ftok failed\n");
		return -1;
	}
	
	int shmid = shmget(key, sizeof(sharedStruct), IPC_CREAT | 0777);
	
	if (shmid == -1){
		perror("Error in child shmget");
		exit(1);
	}
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));
	
	shmP->clockSeconds = 0;
	shmP->clockMilliseconds = 0;
	shmP->processes = 0;
	for (i = 0; i < MAX_PROCESSES; i++){
		shmP->bitVector[i] = 0;	
	}
	
	/*** Create the semaphores for the processes ***/
	for(i = 0; i < NUM_PROCESSES; i++) {
		sprintf(semaphoreName, "damUser%d", i);

		processSem[i] = sem_open (semaphoreName, O_CREAT, 0777, 1);
		if (processSem[i] == SEM_FAILED){
				printf("Problem creating semaphore for process %d \n", i);
		}
	}

	
	/*** Start forking processes ***/
	while(1){ 
		printf("process: %d\ttotal: %d\n", shmP->processes, total);	

		ms = rand() % 1000 + 1; //Get a number between 1 - 1000
		incrementTime(1, ms);
//		while(shmP->processes == NUM_PROCESSES);

		if (total < MAX_PROCESSES) { //If total created processes is less than MAX
			if(shmP->processes < NUM_PROCESSES){
				int j, k;
				for (j = 0; j < shmP->processes; j++){
					if (shmP->bitVector[j] == 0){
						k = j;
						break;
					}
				}
			
				shmP->bitVector[k] = 1;
			
				shmP->pidArray[k] = fork(); //Assign the child PID to array
				if (shmP->pidArray[k] == 0){
					sprintf(userArg, "%d", k);
					printf("%s\n", userArg);
					execl("./user", "user", userArg, NULL);
				}
				

				shmP->processes++;
				total++;
			}
		}
	}
	
	return 1;
}
