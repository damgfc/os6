CC = gcc
CFLAGS =  
OBJSM = oss.o shared.o
OBJSC = user.o shared.o
PROGM = oss 
PROGC = user 

.SUFFIXES: .c .o

all: $(PROGM) $(PROGC)

$(PROGM): $(OBJSM)
	$(CC) -g -o $@ $(OBJSM) -lpthread

$(PROGC): $(OBJSC)
	$(CC) -g -o $@ $(OBJSC) -lpthread

shared.o: shared.c
	$(CC) -g -c shared.c

.c.o:
	$(CC) -c -o $@ $<
	
clean:
	rm *.o 
	rm $(PROGM) $(PROGC) 
