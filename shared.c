/*
$Author: o-moring $
$Log: shared.c,v $
Revision 1.2  2015/04/30 01:09:18  o-moring
fixed issues with shm & added semaphores

Revision 1.1  2015/04/20 04:19:37  o-moring
Initial revision

$Date: 2015/04/30 01:09:18 $
*/

#include "shared.h"

sharedStruct *shmP;
int shmid;

int createSharedMemory(){
	int key = ftok("./ftokFile", 42);
	if (key == -1){
		perror("ftok failed\n");
		return -1;
	}
	
	shmid = shmget(key, sizeof(sharedStruct), 0777);
	
	if (shmid == -1){
		perror("Unsuccesful Shmget");
		exit(1);
	}
	else {
		shmP = (sharedStruct *)(shmat(shmid, 0, 0));
		if (shmP == (void *)-1){
			return -1;
		}
	}
}

int detatchAndRemove(int shmid, void *shmaddr){
	int err = 0;
	
// 	retVal = shmdt(shmP);
	
	if (shmdt(shmP) == -1){
		err = errno; 
		perror("Problem detaching shared memory");
	}
	if ((shmctl(shmid, IPC_RMID, NULL) == 1) && !err)	{
		err = errno;
		perror("Problem detaching shared memory");
	}
	if (!err){
		return 0;
	}
	
	err = errno;
	return -1;
}

void signalHandling(int sigNum){
	int i = 0;
		
	if (sigNum == SIGINT){
		for (i = 0; i < NUM_PROCESSES; i++){
			kill(shmP->pidArray[i], SIGINT);
			//sem_close(semaphores[i]);
		}
	
		detatchAndRemove(shmid, shmP);
		exit(sigNum);
	}
	else {
		for (i = 0; i < NUM_PROCESSES; i++){
			//sem_close(semaphores[i]);
		}
	
		detatchAndRemove(shmid, shmP);	
		exit(sigNum);
	}	
	
}

void incrementTime(unsigned int seconds, unsigned int milliseconds){
	shmP->clockMilliseconds += milliseconds;
	
	while (shmP->clockMilliseconds >= 1000){
		shmP->clockMilliseconds -= 1000;
		shmP->clockSeconds += 1;
	}
	
	shmP->clockSeconds += seconds;
}
